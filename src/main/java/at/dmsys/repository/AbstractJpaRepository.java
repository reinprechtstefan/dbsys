package at.dmsys.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractJpaRepository<T> implements JpaRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public AbstractJpaRepository() {
	}

	protected EntityManager entityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public abstract List<T> findAll();

	public abstract T findById(Long id);

	public void persist(T entity) {
		entityManager.persist(entity);
	}
}
