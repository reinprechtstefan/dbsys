package at.dmsys.domain;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Date {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String startplace;
	private String endplace;
	private ArrayList<Customer> customers;
	private Date date;

	public Date() {
	}
	
	public Date(int id, String startplace, String endplace, ArrayList<Customer> customers, Date date) {
		this.id = id;
		this.startplace = startplace;
		this.endplace = endplace;
		this.customers = customers;
		this.date = date;
	}

	public String getStartplace() {
		return startplace;
	}

	public void setStartplace(String startplace) {
		this.startplace = startplace;
	}

	public String getEndplace() {
		return endplace;
	}

	public void setEndplace(String endplace) {
		this.endplace = endplace;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}
}
